const fs = require('fs');

/**
 *
 * @param filePath
 * @returns true if Exist or false if not
 */

const isFileExists = filePath => {
    try {
        return fs.statSync(filePath).isFile();
    } catch (err) {
        return false;
    }
};

module.exports = { isFileExists };
