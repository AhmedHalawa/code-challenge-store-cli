const fs = require('fs');
const path = require('path');
const { isFileExists } = require('./helper');
const crud = require('./crud'); // crud logic

const filePath = path.join(__dirname, 'data.json');

if (isFileExists(filePath)) {
    let data = fs.readFileSync(filePath);
    let dataParse = JSON.parse(data); // parse from json to jso

    /**
     * process.argv[0] is node
     * process.argv[1] is store or store.js
     * process.argv[2] is command options
     */
    switch (process.argv[2]) {
        case 'add':
            try {
                let dataAfterAdded = crud.add(
                    dataParse,
                    process.argv[3],
                    process.argv[4]
                );
                fs.writeFileSync(filePath, JSON.stringify(dataAfterAdded));
            } catch (err) {
                console.log(err.message);
            }
            break;

        case 'list':
            console.log(dataParse);
            break;

        case 'get':
            try {
                crud.get(dataParse, process.argv[3]);
            } catch (err) {
                console.log(err.message);
            }
            break;

        case 'remove':
            try {
                let dataAfterRemove = crud.remove(dataParse, process.argv[3]);
                fs.writeFileSync(filePath, JSON.stringify(dataAfterRemove));
            } catch (err) {
                console.log(err.message);
            }
            break;

        case 'clear':
            fs.writeFileSync(filePath, JSON.stringify({})); // reset to empty object
            console.log('Data is cleared!'); 
            break;
    }
} else {
    console.log('File not exist.');
}
