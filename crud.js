/**
 * ADD - REMOVE - GET
 * Validations
 */
const crud = {
    add: (obj, key, value) => {
        if (key && value) {
            obj[key] = value;
            return obj;
        } else {
            throw new Error(`key and value are required.`);
        }
    },
    get: (obj, key) => {
        if (obj[key]) {
            console.log(obj[key]);
        } else {
            throw new Error(`${key} does't exist.`);
        }
    },
    remove: (obj, key) => {
        if (obj[key]) {
            delete obj[key];
            return obj;
        } else {
            throw new Error(`${key} does't exist to remove.`);
        }
    }
};

module.exports = crud;
